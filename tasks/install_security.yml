---
- name: "install_security | Uninstall ModSecurity Headers module"
  when: "not (install_apache__supplementary_mods.security.enabled | default(false))"
  notify: "Restart Apache2"
  block:
    - name: "install_security | Disable Apache2 Security module"
      community.general.apache2_module:
        state: "absent"
        name: "security2"

    - name: "install_security | Check /etc/apache2/mods-enabled/modsecurity.conf"
      register: "file_check"
      ansible.builtin.stat:
        path: "/etc/apache2/mods-enabled/modsecurity.conf"

    - name: "install_security | Remove /etc/apache2/mods-enabled/modsecurity.conf"
      when: "file_check.stat.exists"
      ansible.builtin.file:
        path: "/etc/apache2/mods-enabled/modsecurity.conf"
        state: "absent"

    - name: "install_security | Check /etc/apache2/mods-enabled/security2.conf"
      register: "file_check"
      ansible.builtin.stat:
        path: "/etc/apache2/mods-enabled/security2.conf"

    - name: "install_security | Remove /etc/apache2/mods-enabled/security2.conf"
      when: "file_check.stat.exists"
      ansible.builtin.file:
        path: "/etc/apache2/mods-enabled/security2.conf"
        state: "absent"

    - name: "install_security | Remove /etc/apache2/mods-enabled/security2.load"
      when: "file_check.stat.exists"
      ansible.builtin.file:
        path: "/etc/apache2/mods-enabled/security2.load"
        state: "absent"

- name: "install_security | Install ModSecurity Headers module"
  when: "install_apache__supplementary_mods.security.enabled | default(false)"
  notify: "Restart Apache2"
  block:
    - name: "install_security | Install libapache2-mod-security2"
      retries: 3
      delay: 10
      until: "output is success"
      register: "output"
      ansible.builtin.package:
        update_cache: true
        name:
          - "libapache2-mod-security2"

    - name: "install_security | Import ModSecurity confiruations files"
      loop:
        - { src: "modsecurity.conf.j2", dest: "/etc/apache2/mods-available/modsecurity.conf"}
        - { src: "security2.conf.j2", dest: "/etc/apache2/mods-available/security2.conf"}
      loop_control:
        loop_var: "file"
      ansible.builtin.template:
        src: "{{ file.src }}"
        dest: "{{ file.dest }}"
        owner: "{{ install_apache.system_user }}"
        group: "{{ install_apache.system_group }}"
        mode: "0755"

    - name: "install_security | Check directory /etc/apache2/modsecurity-crs/coreruleset-{{ install_apache__supplementary_mods.security.core_version }}"
      register: "file_check"
      ansible.builtin.stat:
        path: "/etc/apache2/modsecurity-crs/coreruleset-{{ install_apache__supplementary_mods.security.core_version }}"

    - name: "install_security | Create /etc/apache2/modsecurity-crs directory"
      when: "not file_check.stat.exists"
      ansible.builtin.file:
        path: "/etc/apache2/modsecurity-crs/coreruleset-{{ install_apache__supplementary_mods.security.core_version }}"
        state: "directory"
        recurse: true
        owner: "{{ install_apache.system_user }}"
        group: "{{ install_apache.system_group }}"
        mode: "0755"

    - name: "install_security | Download sources and extract"
      changed_when: false
      ansible.builtin.unarchive:
        src: "https://github.com/coreruleset/coreruleset/archive/refs/tags/v{{ install_apache__supplementary_mods.security.core_version }}.zip"
        dest: "/etc/apache2/modsecurity-crs"
        owner: "{{ install_apache.system_user }}"
        group: "{{ install_apache.system_group }}"
        mode: "0755"
        remote_src: true
        exclude:
          - "coreruleset-{{ install_apache__supplementary_mods.security.core_version }}/crs-setup.conf"

    - name: "install_security | Import CRS confiruations files"
      ansible.builtin.copy:
        src: "crs-setup.conf"
        dest: "/etc/apache2/modsecurity-crs/coreruleset-{{ install_apache__supplementary_mods.security.core_version }}/crs-setup.conf"
        owner: "{{ install_apache.system_user }}"
        group: "{{ install_apache.system_group }}"
        mode: "0755"

    - name: "install_security | Import unicode.mapping file from github"
      ansible.builtin.get_url:
        url: "https://raw.githubusercontent.com/SpiderLabs/ModSecurity/v{{ install_apache__supplementary_mods.security.version }}/unicode.mapping"
        dest: "/etc/apache2/modsecurity-crs/coreruleset-{{ install_apache__supplementary_mods.security.core_version }}/unicode.mapping"
        owner: "{{ install_apache.system_user }}"
        group: "{{ install_apache.system_group }}"
        mode: "0755"

    - name: "install_security | Enable Apache2 Security module"
      community.general.apache2_module:
        state: "present"
        name: "security2"
